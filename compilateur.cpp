//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>
#include <map>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPES {BOOLEAN, INTEGER};

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string


map<string, enum TYPES> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

enum TYPES Identifier(void){
	enum TYPES type;
	if(!IsDeclared(lexer->YYText())){
		cerr<<"Variable"<<lexer->YYText()<<"non déclarée";
	}
	type=DeclaredVariables[lexer->YYText()];
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return type;
}

enum TYPES Number(void){
	unsigned int *i;
	double d;
	bool decimal = false;
	string number = lexer->YYText();
	if(number.find(".") != string::npos){
		i=(unsigned int *) &d;
		cout<<"\tsubq $8,%rsp"<<endl;
		cout<<<"\tmovl $"<<*i<<",(%rsp)"<<endl;
		cout<<"\tmovl $"<<*(i+1)<<",4(%rsp)"<<endl
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else{
		cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
		current=(TOKEN) lexer->yylex();
		return INTEGER;
	}
}

enum TYPES Char(void){
	cout<<"\tmovq $0, %rax"<<endl;
	cout<<"\tmovb $"<<*i<<"%al"<<lexer->yylex()<<endl;
	cout<<"\tpush %rax"<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return CHAR;

}

enum TYPES Expression(void);			// Called by Term() and calls Term()
enum TYPES Factor(void){
	enum TYPES type;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		type=Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else
		if (current==NUMBER)type=Number();
			if(current==ID)type=Identifier();
				if(current==CHARCONST)type=Char();
					else Error("'(' ou chiffre ou lettre attendue");
	return type;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
enum TYPES Term(void){
	TYPES type, type2;
	OPMUL mulop;
	type=Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		type2=Factor();
		if(type2!=type){
			Error("Les appellants suivants de Factor ne renvoie pas le même type");
		}
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(mulop){
			case AND:
				if(type2!=BOOLEAN){
					Error("Type Boolean attendu");
				}
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				if(type2!=INTEGER){
					Error("Type Integer attendu");
				}
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# MUL"<<endl;	// store result
				break;
			case DIV:
				if(type2!=INTEGER){
					Error("Type Integer attendu");
				}
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator
				cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
				cout << "\tpush %rax\t# DIV"<<endl;		// store result
				break;
			case MOD:
				if(type2!=INTEGER){
					Error("Type Integer attendu");
				}
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return type;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
enum TYPES SimpleExpression(void){
	TYPES type, type2;
	OPADD adop;
	type=Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		type2=Term();
		if(type2!=type){
			Error("Les appellants suivants de Term ne renvoie pas le même type");
		}
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(adop){
			case OR:
				if(type2!=BOOLEAN){
					Error("Type Boolean attendu");
				}
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				break;
			case ADD:
				if(type2!=INTEGER){
					Error("Type Integer attendu");
				}
				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				break;
			case SUB:
				if(type2!=INTEGER){
					Error("Type Integer attendu");
				}
				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				break;
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;			// store result
	}
	return type;
}


// VarDeclaration := Ident {"," Ident} ":" Type
void VarDeclaration(void){
	if(current!=ID){
		Error("Un identificateur est attendu");
	}
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
		{
			Error("Un identificateur est attendu");
		}
		current=(TOKEN) lexer->yylex();
		if(current!=COLON){
			Error("Les ':' sont attendus");
		}
		current=(TOKEN) lexer->yylex();
	}
}
// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclarationPart(void){
	current=(TOKEN) lexer->yylex();
	VarDeclaration();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		VarDeclaration();
	}
	if(current!=DOT){
		Error("Le '.' est attendu");
	}
	current=(TOKEN) lexer->yylex();
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
enum TYPES Expression(void){
	TYPES type,type2;
	OPREL oprel;
	type=SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		type2=SimpleExpression();
		if(type2!=type){
			Error("Les appellants suivants de SimpleExpression ne renvoie pas le même type");
		}
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;
		cout << "Suite"<<TagNumber<<":"<<endl;
	}
	return type;
}

// AssignementStatement := Identifier ":=" Expression
void AssignementStatement(void){
	TYPES type, type2;
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	type=DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	type2=Expression();
	if(type2!=type){
		Error("Le type de lavariable et le type de l'expression ne sont pas les mêmes");
	}
	cout<<"\tpop "<<variable<<endl;
}

void Statement(void);
void IfStatement(void)
{
	TYPES type;
	unsigned long long int tag =++TagNumber;
	current=(TOKEN)lexer->yylex();
	type=Expression();
	if(type!=BOOLEAN){
		Error("L'expression n'est pas de type boolean");
	}
	if(current!= KEYWORD && strcmp(lexer->YYText(),"THEN")!=0)
	{
		Error("THEN était attendu");
	}
	current=(TOKEN)lexer->yylex();
	cout<<"\tpop %rax \t"<<endl;
	cout<<"\tje ELSE"<<tag<<endl;
	Statement();
	cout<<"\tjmp Suite"<<tag<<":"<<endl;
	cout<<"ELSE : "<<endl;
	if(current==KEYWORD && strcmp(lexer->YYText(),"ELSE")==0)
	{
		Statement();
	}
	cout<<"Suite"<<tag<<":"<<endl;
}

void CaseStatement(void){
	TYPES type;
	unsigned long long int tag =++TagNumber;
	current=(TOKEN)lexer->yylex();
	type=Expression();
	if(current==KEYWORD && strcmp(lexer->YYText(),"OF")!=0)
	{
		Error("OF  était attendu");
	}
	CaseListElement();
	current=(TOKEN) lexer->yylex();
	while(strcmp(lexer->YYText(),"END")!=0){
		CaseListElement();
	}
	cout<<"\tpop %rax"<<endl;
	cout<<"\tcmpq ,%rax"

}

void WhileStatement(void)
{
	TYPES type;
	unsigned long long int tag=++TagNumber;
  cout<<"While"<<tag<<":"<<endl;
	current=(TOKEN)lexer->yylex();
  type=Expression();
	if(type!=BOOLEAN){
		Error("L'expression n'est pas de type boolean");
	}
  cout<<"\tpop %rax"<<endl;
  cout<<"\tcmpq $0, %rax"<<endl;
  cout<<"\tje EndWhile"<<tag<<endl;
	if(current!=KEYWORD && strcmp(lexer->YYText(),"DO")!=0)
	{
		Error("DO était attendu");
	}
	current=(TOKEN)lexer->yylex();
	Statement();
	cout<<"\tjmp While"<<tag<<":"<<endl;
	cout<<"\tEndWhile :"<<endl;
}

/*void ForStatement(void)
{
	unsigned long long int tag=++TagNumber;
	current=(TOKEN)lexer->yylex();
	AssignementStatement();
	if(current!=KEYWORD || strcmp(lexer->YYText(),"TO")!=0)
	{
		Error("To était attendu");
	}
	current=(TOKEN)lexer->yylex();
	Expression();
	cout<<"\tpop %rax"<<endl;

}*/

void BlockStatement(void)
{
  current=(TOKEN)lexer->yylex();
  Statement();
  while(current==SEMICOLON)
  {
    current=(TOKEN)lexer->yylex();
    Statement();
  }
  if(current!=KEYWORD && strcmp(lexer->YYText(),"END")!=0)
  {
    Error("END était attendu");
  }
  current=(TOKEN)lexer->yylex();
}

void Display(void){
	TYPES type;
	current=(TOKEN) lexer->yylex();
	type=Expression();
	if(type==INTEGER)
	{
		cout<<"\tpop %rdx"<<endl;
		cout<<"\tmovq $FormatString1, %rsi"<<endl;
	}
	else{
		if(type==BOOLEAN){
			cout<<"\tpop %rdx"<<endl;
			cout<<"\tcmpq $0, %rdx"<<endl;
			cout<<"\tje False"<<TagNumber<<endl;
			cout<<"\tmovq $TrueString, %rsi"<<endl;
			cout<<"\tjmp Suiv"<<endl;
			cout<<"\tFalse"<<TagNumber<<":"<<endl;
			cout<<"\tmovq $FalseString, %rsi"<<endl;
			cout<<"\tSuiv"<<TagNumber<<":"<<endl;
		}
	}
}



// Statement := AssignementStatement
void Statement(void){
	if(current==KEYWORD)
	{
	  if(strcmp(lexer->YYText(),"IF")==0)
	  {
	    IfStatement();
	  }
	  else if(strcmp(lexer->YYText(),"WHILE")==0)
	  {
	    WhileStatement();
	  }
	  /*else if(strcmp(lexer->YYText(),"FOR")==0)
	  {
	    ForStatement();
	  }*/
	  else if(strcmp(lexer->YYText(),"BEGIN")==0)
	  {
	    BlockStatement();
	  }
		else if(strcmp(lexer->YYText(), "CASE")==0)
		{
			CaseStatement();
		}
	  else {
	    Error("mot clé atendu");
	  }
	}
	else {
	  if(current==ID)
	  {
	    AssignementStatement();
	  }
	  else {
	    Error("instruction attendu");
	  }
	}
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==RBRACKET)
		DeclarationPart();
	StatementPart();
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
